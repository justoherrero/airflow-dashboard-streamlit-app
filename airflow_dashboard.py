import os
import json
import logging
import requests
import pandas as pd
import streamlit as st
import plotly.express as px

from typing import Dict, List
from collections import Counter
from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta


def get_dags(only_active: str = "false") -> List:
    """
    Returns the dag list for a given tag in Airflow.

    Parameters
    ----------
    only_active: str
        Represents the flag to get only active dags.

    Returns
    -------
    : List
        Contains a list of the Airflow dags with the selected tag.
    """
    dags = []
    offset = 0
    # Initial run
    response_dags = [0]
    while response_dags:
        response = requests.get(
            f'{os.environ["AIRFLOW_API_URL"]}/dags'
            f"?offset={offset}&only_active={only_active}",
            auth=HTTPBasicAuth(
                os.environ["AIRFLOW_USER"], os.environ["AIRFLOW_PASSWORD"]
            ),
        )
        response_dags = response.json()["dags"]
        dags = dags + response_dags
        offset += 100
    return dags


def get_last_dag_run(dag: Dict) -> Dict:
    """
    Given a dag, returns the dag runs using the today's date.

    Parameters
    ----------
    dag: Dict
        Contains the Dag relevant information.

    Returns
    -------
    :Dict
        Contains the Dag runs information for the dag selected.
    """
    logging.info(f'Getting data for the dag: {dag["dag_id"]}')
    api_url = (
        f'{os.environ["AIRFLOW_API_URL"]}/dags/{dag["dag_id"]}'
        "/dagRuns?start_date_gte="
        f'{(datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")}'
        "&limit=1&order_by=-start_date"
    )
    return requests.get(
        api_url,
        auth=HTTPBasicAuth(
            os.environ["AIRFLOW_USER"], os.environ["AIRFLOW_PASSWORD"]),
    ).json()


def get_grouped_dag_runs_by_hour(dag_runs: List):
    """
    Given a list of dag runs data,
    returns a grouped dataset per hour with the
    count for each group

    Parameters
    ----------
    dag_runs: List
        Contains the executions of active dags.

    Returns
    -------
    : pd.Dataframe
        Contains the grouped count of each execution hours
        in the last day.
    """
    time_dags = pd.DataFrame(
        [
            {"dag_hour": datetime.fromisoformat(
                dag["dag_runs"][0]["start_date"]).hour}
            for dag in dag_runs
        ]
    )

    return time_dags.groupby(["dag_hour"])["dag_hour"].count().sort_values()


def get_dag_duration(start_date: datetime, end_time: datetime):
    """
    Given two timestamps, returns the difference
    between them in Hours and minutes

    Parameters
    ----------
    start_date: datetime
        Represents the start execution date
    end_date: datetime
        Represents the end execution date

    Returns
    -------
    : str
        Represents the data previously calculated.
    """
    start_date = datetime.fromisoformat(start_date)
    if end_time:
        end_date = datetime.fromisoformat(end_time)
    else:
        end_date = datetime.fromisoformat(
            datetime.today().isoformat() + "+00:00")

    time_difference = end_date - start_date

    hours = int(time_difference.total_seconds() // 3600)
    minutes = int((time_difference.total_seconds() % 3600) // 60)

    return f"{hours}Hours {minutes}Min"


def format_table_data(dag: Dict):
    """
    Parse the dag data into tabular format.

    Parameters
    ----------
    dag: Dict
        Contains the dag runs information.

    Returns
    -------
    : Dict
        Contains the required format.
    """
    return {
        "Dag ID": dag["dag_runs"][0]["dag_id"],
        "Start Time": dag["dag_runs"][0]["start_date"],
        "End Time": dag["dag_runs"][0]["end_date"],
        "State": dag["dag_runs"][0]["state"],
        "Duration": get_dag_duration(
            dag["dag_runs"][0]["start_date"], dag["dag_runs"][0]["end_date"]
        ),
    }


def main() -> None:
    """
    Main function for our code.

    Parameters
    ----------
    No parameters required

    Returns
    -------
    None
    """

    st.set_page_config(
        page_icon="📊", page_title="Airflow Dashboard", layout="wide"
    )

    # Page header
    st.write(
        '<span style="font-size: 78px; line-height: 1">📊</span>',
        unsafe_allow_html=True,
    )
    st.title("Airflow Platform Status")

    with st.spinner("We are setting up this for you... 😉"):
        dags = get_dags()
        dag_runs = [get_last_dag_run(dag) for dag in dags]
        # Cleans empty runs
        dag_runs = [
            {"dag_runs": dag["dag_runs"],
                "total_entries": dag["total_entries"]}
            for dag in dag_runs
            if dag["dag_runs"]
        ]
        state_count = Counter(element["dag_runs"][0]["state"]
                              for element in dag_runs)

    with open("dag_runs.json", "w") as file:
        file.write(json.dumps(dag_runs, indent=4))

    # Metrics component
    col1, col2, col3, col4 = st.columns(4)

    with col1:
        st.metric(
            label="Failed Dags",
            value=state_count["failed"],
            delta=f"{round((state_count['failed']/len(dag_runs))*100)} %",
            delta_color="inverse",
        )
    with col2:
        st.metric(
            label="Sucess Dags",
            value=state_count["success"],
            delta=f"{round((state_count['success']/len(dag_runs))*100)} %",
        )
    with col3:
        st.metric(
            label="Running Dags",
            value=state_count["running"],
            delta=f"{round((state_count['running']/len(dag_runs))*100)} %",
        )
    with col4:
        st.metric(label="Total Dags", value=len(dag_runs))

    grouped = get_grouped_dag_runs_by_hour(dag_runs)

    st.header("Dags execution distribution by hours")
    fig = px.bar(
        grouped,
        x=grouped.index,
        y=grouped,
        labels={
            "y": "Total dags",
            "index": "Hour (in 24h format)",
        },
    )
    fig.update_layout(
        yaxis=dict(tickmode="linear", dtick=1),
        xaxis=dict(tickmode="linear", dtick=1)
    )
    st.plotly_chart(fig, use_container_width=True)

    # Chart and table components
    col1, col2, col3 = st.columns(3)

    with col1:
        st.header("Success Dags Details")
        st.info(
            "Use the search command to find something in the table",
            icon="ℹ️"
        )
        st.dataframe(
            pd.DataFrame(
                [
                    format_table_data(dag)
                    for dag in dag_runs
                    if "success" in dag["dag_runs"][0]["state"]
                ]
            )
        )
    with col2:
        st.header("Failed Dags Details")
        st.info(
            "Use the search command to find something in the table",
            icon="ℹ️"
        )
        st.dataframe(
            pd.DataFrame(
                [
                    format_table_data(dag)
                    for dag in dag_runs
                    if "failed" in dag["dag_runs"][0]["state"]
                ]
            )
        )
    with col3:
        st.header("Running Dags Details")
        st.info(
            "Use the search command to find something in the table",
            icon="ℹ️"
        )
        st.dataframe(
            pd.DataFrame(
                [
                    format_table_data(dag)
                    for dag in dag_runs
                    if "running" in dag["dag_runs"][0]["state"]
                ]
            )
        )


if __name__ == "__main__":
    logging.info("Main program starts")
    main()
    logging.info("Main program finishes")
