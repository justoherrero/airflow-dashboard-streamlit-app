# Airflow Dashboard - Streamlit App

## Description

This script allows you to check the status of your Airflow platform. Using the Airflow API, this tool lists all the active DAGs, showing their status (successful, failed or running) and how long each DAG run took to execute.

In addition, the data is presented in a graphical format, showing a breakdown of how many runs were completed successfully, failed, or are currently running.

## How to use

1. Ensure that you have an Airflow instance running.
2. Clone this repository.
3. Install the required libraries using pip install -r requirements.txt.
4. Set up your Airflow API credentials by setting AIRFLOW_API_URL, AIRFLOW_USER, and AIRFLOW_PASSWORD as environment variables.
5. Run the script with: `streamlit run airflow-dashboard.py`

## Dependencies

- requests
- pandas
- streamlit
- plotly

## Contact

If you have any question, you can contact me directly to `jherreroa23@outlook.es`

## Acknowledgements

This project was inspired by the need to check and monitor the status of Airflow DAGs quickly and efficiently. Special thanks to the Streamlit and Plotly Express teams for making it so easy to create and visualize data with Python.
